## Tree2

## Установка и настройка

1. **Клонирование репозитория**
- `https://gitlab.com/nozdrachov.oleg/solomonotreenorec.git`

2. **Настройка Docker**

Установите [Docker](https://www.docker.com/) и [Docker Compose](https://docs.docker.com/compose/), если у вас их еще нет.

3. **Запуск контейнеров Docker**
- `docker compose up --build -d`

4. **Подключитесь к бд**
-  `        MYSQL_DATABASE: 'TestTree2'
            MYSQL_USER: 'Treelogin2'
            MYSQL_PASSWORD: 'Treeparol2'
    `

5. **Запустите скрипт**
- `docker compose run --rm php-cli-container php /app/build_tree.php`

6. **Проверка работу скрипта (для удобста я вынес результат в файл json)**

- Откройте файл tree.json (он расположен в корне проекта, возле yml файла).
