<?php
$startTime = microtime(true);

$pdo = new PDO('mysql:host=mysql-container;dbname=TestTree2', 'Treelogin2', 'Treeparol2');

// Получение всех категорий
$stmt = $pdo->query('SELECT * FROM categories');
$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

function buildTree($categories) {
    $tree = [];
    $references = [];

    foreach ($categories as $category) {
        $id = $category['categories_id'];
        $parentId = $category['parent_id'] ?: 0;

        if (!isset($references[$id])) {
            $references[$id] = ['children' => []];
        }

        if ($parentId == 0) {
            $tree[$id] = &$references[$id];
        } else {
            if (!isset($references[$parentId])) {
                $references[$parentId] = ['children' => []];
            }
            $references[$parentId]['children'][$id] = &$references[$id];
        }
    }

    // Преобразование
    $formattedTree = [];
    foreach ($tree as $id => $data) {
        $formattedTree[$id] = formatTree($data['children']);
    }

    return $formattedTree;
}

function formatTree($tree) {
    $formatted = [];
    foreach ($tree as $id => $data) {
        if (empty($data['children'])) {
            $formatted[$id] = $id;
        } else {
            $formatted[$id] = formatTree($data['children']);
        }
    }
    return $formatted;
}

$tree = buildTree($categories);

$endTime = microtime(true);

$executionTime = $endTime - $startTime;

$treeWithExecutionTime = [
    'executionTime' => $executionTime,
    'tree' => $tree
];

$jsonTree = json_encode($treeWithExecutionTime, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

$jsonFilePath = '/app/tree.json';

file_put_contents($jsonFilePath, $jsonTree);

echo "Дерево категорий и время выполнения сохранены в файле 'tree.json'.\n";